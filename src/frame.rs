// Copyright 2016 Robigalia Project Developers
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

use intrusive_collections::IntrusiveRef;
use collections::Vec;
use sel4_sys::seL4_CPtr;
use sel4::{Allocatable, Window, CNodeInfo};
use object_queue::{ObjectQueue, Object, ObjectSet};
#[cfg(target_arch = "x86")]
use sel4::Page;
#[cfg(target_arch = "arm")]
use sel4::SmallPage as Page;


/// A frame managed by the frame allocator.
pub struct Frame {
    obj: IntrusiveRef<Object<seL4_CPtr>>,
}

impl Frame {
    pub fn cptr(&self) -> seL4_CPtr {
        self.obj.object
    }
}

/// The frame allocator manages regions of untyped memory dedicated to eventually
/// becoming pages. When given a new untyped chunk, it immediately splits it into
/// the smallest pages possible. Threaded through these frames is a linked list of
/// all frames tracked by the allocator. There is a free list used for allocating
/// new pages.
/// 
/// This scheme is very simple, and somewhat hurts for efficiency. It does not
/// support large pages or large contiguous allocations. A separate allocator will
/// be needed for those usecases. However, it does support revokation of untyped
/// memory blocks.
pub struct FrameAllocator {
    frames: ObjectQueue<seL4_CPtr, seL4_CPtr>,
}

impl FrameAllocator {
    pub fn new() -> FrameAllocator {
        FrameAllocator {
            frames: ObjectQueue::new()
        }
    }

    /// Add memory to the pool.
    ///
    /// `untyped` should be a cap to an untyped object with at least size
    /// `size_bits`. `window` should contain at least `2**(size_bits-12)` slots.
    /// That is, enough slots for the untyped object to be completely split into
    /// 4K pages.
    pub fn add_memory(&mut self,
                      untyped: seL4_CPtr,
                      mut window: Window,
                      _: &CNodeInfo,
                      size_bits: u8) {
        assert!(size_bits >= 12);
        let num_pages = 1 << (size_bits as usize - 12);
        assert!(window.num_slots >= num_pages);
        window.num_slots = num_pages;
        let set_storage = Vec::with_capacity(num_pages);
        Page::create(untyped, window, 0).unwrap();
        self.frames.add_object_set(ObjectSet::new(set_storage, untyped));
    }

    /// Revoke memory from the pool.
    ///
    /// This causes all frames derived from the given untyped object to be removed from the free
    /// list.
    ///
    /// # Safety
    ///
    /// This function is unconditionally safe. However, it will leak the memory that was used to
    /// store the capabilities internally if there are still frames loaned out which were allocated
    /// from this untyped memory.
    pub fn revoke_untyped(&mut self, untyped: seL4_CPtr) {
        let (free, sets) = unsafe { self.frames.free_and_sets_mut() };
        let mut c = sets.front_mut();
        let mut unlinked_objects: usize = 0;
        while !c.is_null() {
            if c.get().unwrap().metadata == untyped {
                let mut set = c.remove().unwrap();
                for frame in unsafe { set.as_mut().objects.iter_mut() } {
                    if frame.is_linked() {
                        unlinked_objects += 1;
                        unsafe { free.cursor_mut_from_ptr(frame).remove() };
                    }
                }
                if unlinked_objects == set.objects.len() {
                    unsafe {
                        ::core::intrinsics::drop_in_place(set.as_mut());
                    }
                }
                break;
            } else {
                c.move_next();
            }
        }
    }

    /// Get free frames.
    ///
    /// This will push at most `count` free frames into `frames`. For efficiency, `frames` should
    /// have additional capacity for at least `count` elements.
    pub fn get_frames(&mut self, frames: &mut Vec<Frame>, mut count: usize) {
        while let Some(obj) = self.frames.get_object() {
            if count == 0 {
                break;
            }
            frames.push(Frame { obj: obj });
            count -= 1;
        }
    }

    /// Return frames to the pool.
    ///
    /// This will free each frame in `frames`, making it available for future requests.
    ///
    /// # Safety
    ///
    /// If any frame in `frames` is owned by another `FrameAllocator`, undefined behavior will result.
    pub unsafe fn return_frames(&mut self, frames: &[Frame]) {
        for frame in frames {
            self.frames.return_object(frame.obj);
        }
    }

    /// Get a free frame.
    pub fn get_frame(&mut self) -> Option<Frame> {
        self.frames.get_object().map(|o| Frame { obj: o })
    }

    /// Return a frame to the pool.
    ///
    /// # Safety
    ///
    /// If the frame is owned by another `FrameAllocator`, undefined behavior will result.
    pub unsafe fn return_frame(&mut self, frame: Frame) {
        self.frames.return_object(frame.obj);
    }
}
