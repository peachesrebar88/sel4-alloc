// Copyright (c) 2016 The Robigalia Project Developers
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>,
// at your option. All files in the project carrying such
// notice may not be copied, modified, or distributed except
// according to those terms.

use sel4::{self, Window, SlotRef};
use core::cell::Cell;
use sel4::ToCap;
use sel4_sys::seL4_Untyped_Retype;

/// Untyped region tracking.
///
/// Tracks how many objects have been allocated in an untyped region, as well as its size and the
/// number of remaining bytes.
///
/// This roughly mirrors the kernel's internal state about untyped objects, and is useful for
/// knowing whether object allocation into an untyped will be possible. All other utspace
/// allocators come down to managing these buckets.
/// 
/// Note that if deallocation is done without calling `deallocate`, tracking will become
/// desynchronized with the kernel and memory will be wasted.
///
/// Similarly, if allocation is done without informing this object, it will think there is more
/// free memory than there actually is.
#[derive(Debug)]
pub struct UtBucket {
    cap: SlotRef,
    objects: Cell<usize>,
    bytes_used: Cell<usize>,
    total_bytes: usize,
}

impl UtBucket {
    /// Create a new `UtBucket` given a reference to an untyped memory capability and the size_bits
    /// it was created with.
    pub fn new(cap: SlotRef, size_bits: u8) -> UtBucket {
        UtBucket {
            cap: cap,
            objects: Cell::new(0),
            bytes_used: Cell::new(0),
            total_bytes: 1 << (size_bits as usize),
        }
    }

    pub fn has_space(&self, window: Window, size_bits: usize) -> bool {
        let bytes_needed = window.num_slots * size_bits;
        let bytes_used = self.bytes_used.get();
        if bytes_used + (bytes_used % bytes_needed) + bytes_needed > self.total_bytes {
            false
        } else {
            true
        }
    }

    /// Mark `count` objects as deleted.
    ///
    /// If the object count becomes 0, revoke the untyped capability, ensuring it has no children
    /// and can be reused. The return value is the attempt of that revoke, if there was one.
    fn delete(&self, count: usize) -> Option<sel4::Result> {
        self.objects.set(self.objects.get() - count);
        if self.objects.get() == 0 {
            match self.cap.revoke() {
                c @ Ok(_) => {
                    self.bytes_used.set(0);
                    Some(c)
                }
                c => Some(c),
            }
        } else {
            None
        }
    }
}

impl ::UTSpaceManager for UtBucket {
    fn allocate_raw(&self,
                    dest: Window,
                    size_bits: usize,
                    objtype: isize)
                    -> Result<(), (usize, sel4::Error)> {
        println!("Doing an UntypedReptype with dest {:?}, size bits {}, objtype {}", dest, size_bits, objtype);

        let res = unsafe {
            seL4_Untyped_Retype(self.cap.cptr,
                                objtype,
                                size_bits as isize,
                                dest.cnode.root.to_cap(),
                                dest.cnode.cptr as isize,
                                dest.cnode.depth as isize,
                                dest.first_slot_idx as isize,
                                dest.num_slots as isize)
        };
        if res == 0 {
            self.bytes_used.set(self.bytes_used.get() + (self.bytes_used.get() % size_bits) +
                                dest.num_slots * size_bits);
            self.objects.set(self.objects.get() + dest.num_slots);
            Ok(())
        } else {
            Err((0, sel4::Error(sel4::GoOn::CheckIPCBuf)))
        }
    }

    fn deallocate_raw(&self, window: Window, _size_bits: usize) -> sel4::Result {
        match self.delete(window.num_slots) {
            Some(e) => e,
            None => Ok(()),
        }
    }

    fn minimum_slots(&self) -> usize {
        0
    }
    fn minimum_untyped(&self) -> usize {
        0
    }
    fn minimum_vspace(&self) -> usize {
        0
    }
}
