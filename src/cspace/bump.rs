// Copyright (c) 2016 The Robigalia Project Developers
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>,
// at your option. All files in the project carrying such
// notice may not be copied, modified, or distributed except
// according to those terms.


use sel4::{Window, CNodeInfo};
use sel4_sys::seL4_CPtr;
use core::cell::Cell;

/// A "bump allocator" for slots in a window of a CNode.
/// 
/// This records the maximum slot index allocated so far. If a slot is requested, and the CNode is
/// not yet full, the index is incremented. It is possible to free only the most recently requested
/// slot or group of slots.
#[derive(Debug)]
pub struct BumpAllocator {
    /// The portion of CSpace and the CNode which slots are allocated from.
    window: Window,
    info: CNodeInfo,
    /// The watermark number of slots allocated so far.
    watermark: Cell<usize>,
}

/// Slots allocated in bulk from this allocator.
pub struct BumpToken {
    slots: Window,
    watermark: usize,
}

impl ::core::ops::Deref for BumpToken {
    type Target = Window;

    fn deref(&self) -> &Window {
        &self.slots
    }
}

impl BumpAllocator {
    /// Create a new `BumpAllocator` for `window` encoded with `info`.
    pub fn new(window: Window, info: CNodeInfo) -> BumpAllocator {
        BumpAllocator {
            window: window,
            info: info,
            watermark: Cell::new(0),
        }
    }

    /// The window managed by this allocator.
    pub fn window(&self) -> &Window {
        &self.window
    }

    pub fn slots_remaining(&self) -> usize {
        self.window.num_slots - self.watermark.get()
    }
}

impl ::CSpaceManager for BumpAllocator {
    fn allocate_slot<A: ::AllocatorBundle>(&self, _: &A) -> Result<seL4_CPtr, ()> {
        if self.slots_remaining() == 0 {
            Err(())
        } else {
            Ok(self.window
                   .cptr_to(&self.info, self.watermark.get())
                   .expect("tried to allocate a slot out of bounds; should not happen"))
        }
    }

    fn free_slot<A: ::AllocatorBundle>(&self, cptr: seL4_CPtr, _: &A) -> Result<(), ()> {
        let radix = self.info.decode(cptr).radix as usize;
        let watermark = radix - self.window.first_slot_idx;
        if watermark == self.watermark.get() {
            self.watermark.set(self.watermark.get() - 1);
            Ok(())
        } else {
            Err(())
        }
    }

    fn slot_info(&self, _: seL4_CPtr) -> Option<&CNodeInfo> {
        Some(&self.info)
    }

    fn slot_window(&self, cptr: seL4_CPtr) -> Option<Window> {
        println!("{:?}", self.info);
        println!("{} decodes into {:?}", cptr, self.info.decode(cptr));
        println!("");
        let radix = self.info.decode(cptr).radix as usize;
        Some(Window {
            first_slot_idx: radix,
            num_slots: 1,
            ..self.window
        })
    }

    // this manager uses a constant amount of resources.

    fn minimum_slots(&self) -> usize {
        0
    }
    fn minimum_untyped(&self) -> usize {
        0
    }
    fn minimum_vspace(&self) -> usize {
        0
    }
}

impl ::BulkCSpaceManager for BumpAllocator {
    type Token = BumpToken;
    fn allocate_slots<A: ::AllocatorBundle>(&self, count: usize, _: &A) -> Result<BumpToken, ()> {
        if count > self.slots_remaining() {
            Err(())
        } else {
            let mut new = self.window;
            new.first_slot_idx += self.watermark.get();
            new.num_slots = count;
            self.watermark.set(self.watermark.get() + count);
            Ok(BumpToken {
                slots: new,
                watermark: self.watermark.get(),
            })
        }
    }

    fn free_slots<A: ::AllocatorBundle>(&self, token: BumpToken, _: &A) -> Result<(), ()> {
        if token.slots.cnode == self.window.cnode && token.watermark == self.watermark.get() {
            self.watermark.set(self.watermark.get() - token.slots.num_slots);
            Ok(())
        } else {
            Err(())
        }
    }

    fn slots_info(&self, _: &Self::Token) -> Option<&CNodeInfo> {
        Some(&self.info)
    }
}
