// Copyright 2016 Robigalia Project Developers
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

use {CSpaceManager, AllocatorBundle};
use collections::Vec;
use sel4::{Window, CNodeInfo};
use sel4_sys::seL4_CPtr;

/// A two-level tree of CSpace managers.
pub struct TwoLevel<Top: CSpaceManager, Bottom: CSpaceManager> {
    pub top: Top,
    pub bottom: Vec<Bottom>,
}

impl<T: CSpaceManager, B: CSpaceManager> TwoLevel<T, B> {
    pub fn new(top: T) -> TwoLevel<T, B> {
        TwoLevel {
            top: top,
            bottom: Vec::new(),
        }
    }
}

impl<T: CSpaceManager, B: CSpaceManager> CSpaceManager for TwoLevel<T, B> {
    fn allocate_slot<A: AllocatorBundle>(&self, alloc: &A) -> Result<seL4_CPtr, ()> {
        for bot in self.bottom.iter() {
            match bot.allocate_slot(alloc) {
                c @ Ok(_) => return c,
                _ => {}
            }
        }
        Err(())
    }

    fn free_slot<A: AllocatorBundle>(&self, cptr: seL4_CPtr, alloc: &A) -> Result<(), ()> {
        let radix = self.top.slot_info(cptr).unwrap().decode(cptr).radix as usize;
        match self.bottom.get(radix) {
            Some(b) => b.free_slot(cptr, alloc),
            None => Err(()),
        }
    }

    fn slot_info(&self, cptr: seL4_CPtr) -> Option<&CNodeInfo> {
        let radix = self.top.slot_info(cptr).unwrap().decode(cptr).radix as usize;
        match self.bottom.get(radix) {
            Some(b) => b.slot_info(cptr),
            None => None,
        }
    }

    fn slot_window(&self, cptr: seL4_CPtr) -> Option<Window> {
        let radix = self.top.slot_info(cptr).unwrap().decode(cptr).radix as usize;
        match self.bottom.get(radix) {
            Some(b) => b.slot_window(cptr),
            None => None,
        }
    }

    fn minimum_slots(&self) -> usize {
        self.bottom.iter().map(|b| b.minimum_slots()).max().unwrap_or(0)
    }
    fn minimum_untyped(&self) -> usize {
        self.bottom.iter().map(|b| b.minimum_untyped()).max().unwrap_or(0)
    }
    fn minimum_vspace(&self) -> usize {
        self.bottom.iter().map(|b| b.minimum_vspace()).max().unwrap_or(0)
    }
}
