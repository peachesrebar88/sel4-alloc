// Copyright 2016 Robigalia Project Developers
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

use {VSpaceManager, UTSpaceManager, CSpaceManager, BulkCSpaceManager, AllocatorBundle, seL4_ARCH_VMAttributes};
use sel4_sys::{seL4_CPtr, seL4_CapRights};
use collections::Vec;
use sel4;

#[derive(Copy, Clone, Debug)]
pub struct DummyAlloc;

#[allow(unused_variables)]
impl VSpaceManager for DummyAlloc {
    type Reservation = (usize, usize);
    fn map<A: AllocatorBundle>(&self,
                                 caps: &[seL4_CPtr],
                                 size_bits: u8,
                                 rights: seL4_CapRights,
                                 attrs: seL4_ARCH_VMAttributes,
                                 alloc: &A)
                                 -> usize {
        unimplemented!()
    }
    fn map_at_vaddr<A: AllocatorBundle>(&self,
                                          caps: &[seL4_CPtr],
                                          vaddr: usize,
                                          size_bits: u8,
                                          reservation: &Self::Reservation,
                                          rights: seL4_CapRights,
                                          attrs: seL4_ARCH_VMAttributes,
                                          alloc: &A)
                                          -> bool 
    {
        unimplemented!()
    }
    fn change_protection<A: AllocatorBundle>(&self,
                                               vaddr: usize,
                                               bytes: usize,
                                               rights: seL4_CapRights,
                                               attrs: seL4_ARCH_VMAttributes,
                                               alloc: &A)
    {
        unimplemented!()
    }
    fn unmap(&self, vaddr: usize, bytes: usize, caps: Option<&mut Vec<seL4_CPtr>>)
    {
        unimplemented!()
    }
    fn reserve<A: AllocatorBundle>(&self, bytes: usize, alloc: &A) -> Option<Self::Reservation>
    {
        unimplemented!()
    }
    fn reserve_at_vaddr<A: AllocatorBundle>(&self,
                                              vaddr: usize,
                                              bytes: usize,
                                              alloc: &A)
                                              -> Option<Self::Reservation>
    {
        unimplemented!()
    }
    fn unreserve<A: AllocatorBundle>(&self, reservation: Self::Reservation, alloc: &A)
    {
        unimplemented!()
    }
    fn unreserve_at_vaddr<A: AllocatorBundle>(&self, vaddr: usize, alloc: &A) -> Result<(), ()>
    {
        unimplemented!()
    }
    fn get_cap(&self, vaddr: usize) -> Option<seL4_CPtr>
    {
        unimplemented!()
    }
    fn root(&self) -> seL4_CPtr
    {
        unimplemented!()
    }

    fn minimum_slots(&self) -> usize
    {
        unimplemented!()
    }
    fn minimum_untyped(&self) -> usize
    {
        unimplemented!()
    }
    fn minimum_vspace(&self) -> usize
    {
        unimplemented!()
    }
}

#[allow(unused_variables)]
impl CSpaceManager for DummyAlloc {
    fn allocate_slot<A: AllocatorBundle>(&self, allocator: &A) -> Result<seL4_CPtr, ()>
    {
        unimplemented!()
    }
    fn free_slot<A: AllocatorBundle>(&self, cptr: seL4_CPtr, allocator: &A) -> Result<(), ()>
    {
        unimplemented!()
    }
    fn slot_info(&self, cptr: seL4_CPtr) -> Option<&sel4::CNodeInfo>
    {
        unimplemented!()
    }
    fn slot_window(&self, cptr: seL4_CPtr) -> Option<sel4::Window>
    {
        unimplemented!()
    }

    fn minimum_slots(&self) -> usize
    {
        unimplemented!()
    }
    fn minimum_untyped(&self) -> usize
    {
        unimplemented!()
    }
    fn minimum_vspace(&self) -> usize
    {
        unimplemented!()
    }
}

#[allow(unused_variables)]
impl BulkCSpaceManager for DummyAlloc {
    type Token = &'static sel4::Window;
    fn allocate_slots<A: AllocatorBundle>(&self,
                                          count: usize,
                                          allocator: &A)
                                          -> Result<Self::Token, ()>
    {
        unimplemented!()
    }
    fn free_slots<A: AllocatorBundle>(&self, token: Self::Token, allocator: &A) -> Result<(), ()>
    {
        unimplemented!()
    }
    fn slots_info(&self, token: &Self::Token) -> Option<&sel4::CNodeInfo>
    {
        unimplemented!()
    }
}

#[allow(unused_variables)]
impl UTSpaceManager for DummyAlloc {
    fn allocate<T: sel4::Allocatable>(&self,
                                      window: sel4::Window,
                                      size_bits: usize)
                                      -> Result<(), (usize, sel4::Error)> {
        self.allocate_raw(window,
                          T::object_size(size_bits as isize) as usize,
                          T::object_type() as isize)
    }

    fn deallocate<T: sel4::Allocatable>(&self,
                                        window: sel4::Window,
                                        size_bits: usize)
                                        -> sel4::Result {
        self.deallocate_raw(window, T::object_size(size_bits as isize) as usize)
    }
    fn allocate_raw(&self,
                    window: sel4::Window,
                    size_bits: usize,
                    objtype: isize)
                    -> Result<(), (usize, sel4::Error)>
    {
        unimplemented!()
    }

    fn deallocate_raw(&self, window: sel4::Window, size_bits: usize) -> sel4::Result
    {
        unimplemented!()
    }

    fn minimum_slots(&self) -> usize
    {
        unimplemented!()
    }
    fn minimum_untyped(&self) -> usize
    {
        unimplemented!()
    }
    fn minimum_vspace(&self) -> usize
    {
        unimplemented!()
    }
}
