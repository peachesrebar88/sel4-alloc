// Copyright 2016 Robigalia Project Developers
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

#![doc(html_root_url = "https://doc.robigalia.org/")]
#![no_std]
#![feature(collections, alloc, heap_api)]

//! Resource allocation.
//!
//! See the README for design notes.
//!
//! This crate is analogous to libsel4allocman, libsel4vka, libsel4vspace, and portions of
//! libsel4utils from the seL4 C libraries.

#[macro_use]
extern crate sel4;
extern crate sel4_sys;
extern crate bitmap;
#[macro_use]
extern crate collections;
#[macro_use]
extern crate intrusive_collections;
extern crate alloc;

use sel4_sys::{seL4_CPtr, seL4_CapRights};
use collections::Vec;

#[allow(non_camel_case_types)]
#[cfg(all(target_arch = "arm", target_pointer_width = "32"))]
pub type seL4_ARCH_VMAttributes = sel4_sys::seL4_ARM_VMAttributes;
#[allow(non_camel_case_types)]
#[cfg(all(target_arch = "x86", target_pointer_width = "32"))]
pub type seL4_ARCH_VMAttributes = sel4_sys::seL4_IA32_VMAttributes;

/// CSpace management
pub mod cspace {
    mod bump;
    mod bitmap;
    mod two_level;

    pub use self::bump::{BumpAllocator, BumpToken};
    pub use self::bitmap::BitmapAllocator;
    pub use self::two_level::TwoLevel;
}

pub mod utspace;

mod object_queue;

mod frame;
mod dummy;

pub use frame::{FrameAllocator, Frame};
pub use dummy::DummyAlloc;
pub use object_queue::{ObjectQueue, ObjectSet, Object};

/// VSpace management
pub mod vspace {
    mod hier;
    pub use self::hier::Hier;
}

pub mod bootstrap;

/// Resource allocator bundle.
pub trait AllocatorBundle {
    type CSpace: CSpaceManager+core::fmt::Debug;
    type UTSpace: UTSpaceManager+core::fmt::Debug;
    type VSpace: VSpaceManager+core::fmt::Debug;

    fn cspace(&self) -> &Self::CSpace;
    fn utspace(&self) -> &Self::UTSpace;
    fn vspace(&self) -> &Self::VSpace;

    fn minimum_slots(&self) -> usize;
    fn minimum_untyped(&self) -> usize;
    fn minimum_vspace(&self) -> usize;
}

impl<C: CSpaceManager + core::fmt::Debug, UT: UTSpaceManager + core::fmt::Debug, V: VSpaceManager + core::fmt::Debug> AllocatorBundle for (C, UT, V) {
    type CSpace = C;
    type UTSpace = UT;
    type VSpace = V;

    fn cspace(&self) -> &C {
        &self.0
    }
    fn utspace(&self) -> &Self::UTSpace {
        &self.1
    }
    fn vspace(&self) -> &Self::VSpace {
        &self.2
    }

    fn minimum_slots(&self) -> usize {
        self.cspace().minimum_slots() + self.utspace().minimum_slots() +
        self.vspace().minimum_slots()
    }
    fn minimum_untyped(&self) -> usize {
        self.cspace().minimum_untyped() + self.utspace().minimum_untyped() +
        self.vspace().minimum_untyped()
    }
    fn minimum_vspace(&self) -> usize {
        self.cspace().minimum_vspace() + self.utspace().minimum_vspace() +
        self.vspace().minimum_vspace()
    }
}

pub trait VSpaceReservation {
    fn start_vaddr(&self) -> usize;
    fn end_vaddr(&self) -> usize;
}

impl VSpaceReservation for (usize, usize) {
    fn start_vaddr(&self) -> usize {
        self.0
    }
    fn end_vaddr(&self) -> usize {
        self.1
    }
}

/// Manager of a virtual address space.
pub trait VSpaceManager {
    type Reservation: VSpaceReservation;

    fn map<A: ::AllocatorBundle>(&self,
                                 caps: &[seL4_CPtr],
                                 size_bits: u8,
                                 rights: seL4_CapRights,
                                 attrs: seL4_ARCH_VMAttributes,
                                 alloc: &A)
                                 -> usize {
        let res = self.reserve(caps.len() * (1 << size_bits), alloc).unwrap();
        assert!(self.map_at_vaddr(caps,
                                  res.start_vaddr(),
                                  size_bits,
                                  &res,
                                  rights,
                                  attrs,
                                  alloc));
        res.start_vaddr()
    }

    fn map_at_vaddr<A: ::AllocatorBundle>(&self,
                                          caps: &[seL4_CPtr],
                                          vaddr: usize,
                                          size_bits: u8,
                                          reservation: &Self::Reservation,
                                          rights: seL4_CapRights,
                                          attrs: seL4_ARCH_VMAttributes,
                                          alloc: &A)
                                          -> bool;

    /// Change the protection on all pages mapped starting at `vaddr` going for `bytes` to `rights`
    /// and `attrs`.
    fn change_protection<A: ::AllocatorBundle>(&self,
                                               vaddr: usize,
                                               bytes: usize,
                                               rights: seL4_CapRights,
                                               attrs: seL4_ARCH_VMAttributes,
                                               alloc: &A);

    /// Unmap pages which cover the region starting at `vaddr` going for `bytes` bytes.
    ///
    /// The caps used by the pages will be pushed into `caps`, if it is not `None`.
    fn unmap(&self, vaddr: usize, bytes: usize, caps: Option<&mut Vec<seL4_CPtr>>);

    /// Reserve a region of virtual memory.
    ///
    /// This will reserve at least `bytes` worth of virtual memory, possibly rounded up to some
    /// multiple of some page size.
    fn reserve<A: ::AllocatorBundle>(&self, bytes: usize, alloc: &A) -> Option<Self::Reservation>;

    /// Reserve a region of virtual memory at a specific address.
    ///
    /// This will fail if the requested region overlaps an existing reservation somewhere.
    ///
    /// This will reserve at least `bytes` worth of virtual memory, possibly rounded up to some
    /// multiple of some page size.
    fn reserve_at_vaddr<A: ::AllocatorBundle>(&self,
                                              vaddr: usize,
                                              bytes: usize,
                                              alloc: &A)
                                              -> Option<Self::Reservation>;

    /// Unreserve a region.
    fn unreserve<A: ::AllocatorBundle>(&self, reservation: Self::Reservation, alloc: &A);

    /// Unreserve a region given a pointer into it.
    ///
    /// `vaddr` can be any address in a region, it does not need to be the start address.
    fn unreserve_at_vaddr<A: ::AllocatorBundle>(&self, vaddr: usize, alloc: &A) -> Result<(), ()>;

    /// Get the cap mapped in at an address.
    fn get_cap(&self, vaddr: usize) -> Option<seL4_CPtr>;

    /// Get the cap to the top-level paging structure.
    fn root(&self) -> seL4_CPtr;

    fn minimum_slots(&self) -> usize;
    fn minimum_untyped(&self) -> usize;
    fn minimum_vspace(&self) -> usize;
}

/// Manager of individual slots in a CSpace.
pub trait CSpaceManager {
    /// Allocate a single slot from this manager.
    fn allocate_slot<A: AllocatorBundle>(&self, allocator: &A) -> Result<seL4_CPtr, ()>;

    /// Free a previous allocated slot.
    ///
    /// If the named slot has already been freed, or isn't managed by this manager, there may be
    /// unexpected consequences, such as a panic.
    fn free_slot<A: AllocatorBundle>(&self, cptr: seL4_CPtr, allocator: &A) -> Result<(), ()>;

    /// Return a `CNodeInfo` which can be used to decode the given CPtr.
    ///
    /// Can return `None` if the CPtr is not managed by this manager, but is not required to do so.
    fn slot_info(&self, cptr: seL4_CPtr) -> Option<&sel4::CNodeInfo>;

    /// Get a window containing only the slot indicated by `cptr`.
    ///
    /// If the named slot has already been freed, or isn't managed by this manager, there may be
    /// unexpected consequences, such as a panic.
    fn slot_window(&self, cptr: seL4_CPtr) -> Option<sel4::Window>;

    fn minimum_slots(&self) -> usize;
    fn minimum_untyped(&self) -> usize;
    fn minimum_vspace(&self) -> usize;
}

/// Manager of sub-Windows of a CSpace.
pub trait BulkCSpaceManager: CSpaceManager {
    /// This token represents any additional information necessary for later freeing a range.
    type Token: core::ops::Deref<Target = sel4::Window>;

    /// Allocate `count` slots.
    fn allocate_slots<A: AllocatorBundle>(&self,
                                          count: usize,
                                          allocator: &A)
                                          -> Result<Self::Token, ()>;

    /// Free a previously allocated range.
    ///
    /// If the named range has already been freed, or isn't managed by this manager, there may be
    /// unexpected consequences, such as a panic.
    fn free_slots<A: AllocatorBundle>(&self, token: Self::Token, allocator: &A) -> Result<(), ()>;

    /// Return a `CNodeInfo` which can be used to encode CPtrs from the given window.
    ///
    /// Can return `None` if the window is not managed by this manager, but is not required to do
    /// so.
    fn slots_info(&self, token: &Self::Token) -> Option<&sel4::CNodeInfo>;
}

/// Manager of some amount of untyped memory.
pub trait UTSpaceManager {
    /// Allocate a objects into a window.
    ///
    /// The window is entirely filled with objects, so if there are 3 slots in the window, three
    /// objects will be allocated. It is not guaranteed that all objects will come from the same
    /// underlying untyped memory object. If allocation fails, some of the slots may contain
    /// live caps.
    ///
    /// See the manual for the interpretation of `size_bits`. It is only used for CNode and
    /// Untyped Memory objects.
    ///
    /// Returns Ok if allocation of all objects succeeded. Otherwise, returns Err with the number
    /// of objects allocated and the error token of the allocation that failed.
    ///
    /// **NOTE:** failure semantics are not yet finalised.
    fn allocate<T: sel4::Allocatable>(&self,
                                      window: sel4::Window,
                                      size_bits: usize)
                                      -> Result<(), (usize, sel4::Error)> {
        self.allocate_raw(window,
                          T::object_size(size_bits as isize) as usize,
                          T::object_type() as isize)
    }

    /// Deallocate objects from a window.
    ///
    /// Note that deallocation with this method does not guarantee the objects will ever be revoked
    /// or deleted.
    ///
    /// The `size_bits` must be the same that was used to allocate the objects in the
    /// window; this is to allow efficient implementations.
    ///
    /// **NOTE:** failure semantics are not yet finalised.
    fn deallocate<T: sel4::Allocatable>(&self,
                                        window: sel4::Window,
                                        size_bits: usize)
                                        -> sel4::Result {
        self.deallocate_raw(window, T::object_size(size_bits as isize) as usize)
    }

    /// Same semantics as `allocate`, but with the object type given explicitly instead of via a
    /// trait.
    fn allocate_raw(&self,
                    window: sel4::Window,
                    size_bits: usize,
                    objtype: isize)
                    -> Result<(), (usize, sel4::Error)>;

    /// Same semantics as `deallocate`, but with the object type given explicitly instead of via a
    /// trait.
    fn deallocate_raw(&self, window: sel4::Window, size_bits: usize) -> sel4::Result;

    fn minimum_slots(&self) -> usize;
    fn minimum_untyped(&self) -> usize;
    fn minimum_vspace(&self) -> usize;
}
