// Copyright 2016 Robigalia Project Developers
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

use intrusive_collections::{LinkedList, linked_list, IntrusiveRef};
use collections::Vec;
use alloc::boxed::Box;
use core::mem::{forget, transmute};
use core::intrinsics::drop_in_place;
use core::marker::PhantomData;

/// An object managed by an `ObjectQueue`.
pub struct Object<T> {
    free_list: linked_list::Link,
    pub object: T,
}

impl<T> Object<T> {
    pub fn new(object: T) -> Object<T> {
        Object {
            free_list: Default::default(),
            object: object,
        }
    }

    pub fn is_linked(&self) -> bool {
        self.free_list.is_linked()
    }
}

/// Set of objects managed by the `ObjectQueue`.
pub struct ObjectSet<T, U> {
    set_list: linked_list::Link,
    pub objects: Vec<Object<T>>,
    pub metadata: U,
}

impl<T, U> ObjectSet<T, U> {
    pub fn new(objects: Vec<Object<T>>, metadata: U) -> ObjectSet<T, U> {
        ObjectSet {
            set_list: Default::default(),
            objects: objects,
            metadata: metadata,
        }
    }
}

#[derive(Clone, Default)]
pub struct FreeObjects<T>(PhantomData<T>);
#[derive(Clone, Default)]
pub struct ObjectSets<T, U>(PhantomData<T>, PhantomData<U>);

// FIXME: use the macro when https://github.com/Amanieu/intrusive-rs/issues/1 is fixed.

unsafe impl<T> ::intrusive_collections::Adaptor<linked_list::Link> for FreeObjects<T> {
    type Container = Object<T>;
    #[inline]
    unsafe fn get_container(&self, link: *const linked_list::Link) -> *const Object<T> {
        let offset = &(*(0 as *const Object<T>)).free_list as *const _ as isize;
        (link as *const _ as *const u8).offset(-offset) as *mut Object<T>
    }
    #[inline]
    unsafe fn get_link(&self, container: *const Object<T>) -> *const linked_list::Link {
        &(*container).free_list
    }
}

unsafe impl<T, U> ::intrusive_collections::Adaptor<linked_list::Link> for ObjectSets<T, U> {
    type Container = ObjectSet<T, U>;
    #[inline]
    unsafe fn get_container(&self, link: *const linked_list::Link) -> *const ObjectSet<T, U> {
        let offset = &(*(0 as *const ObjectSet<T, U>)).set_list as *const _ as isize;
        (link as *const _ as *const u8).offset(-offset) as *mut ObjectSet<T, U>
    }
    #[inline]
    unsafe fn get_link(&self, container: *const ObjectSet<T, U>) -> *const linked_list::Link {
        &(*container).set_list
    }
}

/// Simple free-list object queue
///
/// The object queue manages sets of objects, which are contained Vecs. Individual objects are
/// threaded through a free list. Requesting or freeing a new object is an O(1) operation.
///
/// It is very important for safety that objects managed by a queue are not returned as free
/// objects to another queue, and that the objects themselves never outlive this queue. Otherwise,
/// deallocating the `ObjectQueue` will cause dangling pointers in all objects which were sourced
/// from this queue.
///
/// For this reason, the destructor of `ObjectQueue` leaks all object sets. In a debug build, it
/// will also panic. Releasing the resources  `ObjectQueue` should be done with the (unsafe)
/// `free_resources` method.
pub struct ObjectQueue<T, U> {
    sets: LinkedList<ObjectSets<T, U>>,
    free: LinkedList<FreeObjects<T>>,
    num_objects_loaned: usize,
}

impl<T, U> ObjectQueue<T, U> {
    pub fn new() -> Self {
        ObjectQueue {
            sets: LinkedList::new(ObjectSets(PhantomData, PhantomData)),
            free: LinkedList::new(FreeObjects(PhantomData)),
            num_objects_loaned: 0,
        }
    }

    /// Free all `ObjectSet`s tracked by this queue.
    ///
    /// This is unsafe if there are any objects currently loaned from the queue. In debug builds,
    /// this will be tracked, and will panic if there are objects loaned.
    pub unsafe fn free_resources(mut self) {
        debug_assert!(self.num_objects_loaned == 0);
        for set in self.sets.iter_mut() {
            drop_in_place(&mut set.objects);
            drop_in_place(&mut transmute::<&mut ObjectSet<T, U>, Box<ObjectSet<T, U>>>(set));
        }
        forget(self)
    }

    /// Direct access to the inner linked list of sets.
    pub unsafe fn sets_mut(&mut self) -> &mut LinkedList<ObjectSets<T, U>> {
        &mut self.sets
    }

    /// Direct access to the inner linked list of free objects.
    pub unsafe fn free_mut(&mut self) -> &mut LinkedList<FreeObjects<T>> {
        &mut self.free
    }

    /// Direct access to the inner linked list of free objects and sets.
    pub unsafe fn free_and_sets_mut
        (&mut self)
         -> (&mut LinkedList<FreeObjects<T>>, &mut LinkedList<ObjectSets<T, U>>) {
        let &mut ObjectQueue { ref mut free, ref mut sets, .. } = self;
        (free, sets)
    }

    /// Add an `ObjectSet` to this queue.
    pub fn add_object_set(&mut self, set: ObjectSet<T, U>) {
        let mut set = IntrusiveRef::from_box(Box::new(set));
        self.sets.cursor_mut().insert_after(set);
        {
            let mut c = self.free.cursor_mut();
            for obj in unsafe { set.as_mut().objects.iter_mut() } {
                c.insert_before(unsafe { IntrusiveRef::from_raw(obj) });
            }
        }
    }

    /// Return an object to the queue.
    ///
    /// # Safety
    ///
    /// If the object is not owned by this queue, undefined behavior will result.
    pub unsafe fn return_object(&mut self, object: IntrusiveRef<Object<T>>) {
        self.free.cursor_mut().insert_after(object);
        if cfg!(debug) {
            self.num_objects_loaned.wrapping_sub(1);
        }
    }

    /// Get an object from the queue.
    pub fn get_object(&mut self) -> Option<IntrusiveRef<Object<T>>> {
        let val = self.free.front_mut().remove();
        if cfg!(debug) {
            if val.is_some() {
                self.num_objects_loaned.wrapping_add(1);
            }
        }
        val
    }
}
