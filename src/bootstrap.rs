// Copyright 2016 Robigalia Project Developers
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

use sel4::{SlotRef, Window, CNodeInfo, CNode, FromCap};
use cspace::BumpAllocator;
use utspace::UtBucket;
use vspace::Hier;

/// Accessor for resources needed to bootstrap an allocator stack.
pub trait BootstrapResources {
    fn untyped(&self) -> (SlotRef, u8);
    fn cnode(&self) -> (Window, CNodeInfo);
}

impl BootstrapResources for ::sel4_sys::seL4_BootInfo {
    fn untyped(&self) -> (SlotRef, u8) {
        (SlotRef {
            root: CNode::from_cap(0x2),
            cptr: self.untyped.start,
            depth: 32,
        }, self.untypedSizeBitsList[0])
    }

    fn cnode(&self) -> (Window, CNodeInfo) {
        println!("root has {} as init cnode size bits", self.initThreadCNodeSizeBits);
        let empty = Window {
            cnode: SlotRef {
                root: CNode::from_cap(0x2),
                cptr: 2,
                depth: 32,
            },
            first_slot_idx: self.empty.start as usize,
            num_slots: (self.empty.end - self.empty.start) as usize,
        };
        let info = CNodeInfo {
            guard_val: 0,
            radix_bits: self.initThreadCNodeSizeBits,
            guard_bits: 32 - self.initThreadCNodeSizeBits,
            prefix_bits: 0,
        };
        (empty, info)
    }
}

/// Bootstrap allocators from initial resources to enable an actual allocator to be able to run.
///
/// # Note
///
/// This requires `alloc` functions to work! The recommended implementation is a simple bump
/// allocator using memory defined in the ELF section.
pub fn bootstrap_allocators<B: BootstrapResources>(bi: &B) -> (BumpAllocator, UtBucket, Hier) {
    let cnode = bi.cnode();
    let bump = BumpAllocator::new(cnode.0, cnode.1);

    let ut = bi.untyped();
    let bucket = UtBucket::new(ut.0, ut.1);
    
    let alloc = (bump, bucket, ::DummyAlloc);
    let hier = Hier::new(&alloc);

    (alloc.0, alloc.1, hier)
}
